using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using sameness_api.Models;

namespace sameness_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EventController : ControllerBase
    {
        private readonly ILogger<EventController> _logger;

        public EventController(ILogger<EventController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Event> Get()
        {
            List<Event> events = new List<Event>();

            events.Add( new Event() { Title = "nor(DEV):con", Date = DateTime.Now, Excerpt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." });
            events.Add( new Event() { Title = "SYnc the City", Date = DateTime.Now, Excerpt = "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat." });
            events.Add( new Event() { Title = "DevelopHER Awards", Date = DateTime.Now, Excerpt = "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur." });
            
            return events.ToArray();
        }
    }
}
