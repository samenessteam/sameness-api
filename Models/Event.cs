
using System;

namespace sameness_api.Models
{
    public class Event
    {
        public string Title {get; set;}
        public DateTime Date {get; set;}
        public string Excerpt {get; set;}
    }
}